//
//  Constants.swift
//  HungNotes
//
//  Created by Hung Cao on 22/5/24.
//

import Foundation

struct Constants {
    static let DATABASE_URL = "https://hung-notes-default-rtdb.asia-southeast1.firebasedatabase.app/"
}
