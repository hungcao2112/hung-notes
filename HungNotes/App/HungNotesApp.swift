//
//  HungNotesApp.swift
//  HungNotes
//
//  Created by Hung Cao on 22/5/24.
//

import SwiftUI

@main
struct HungNotesApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    @State var appRootManager = AppRootManager()
    
    var body: some Scene {
        WindowGroup {
            Group {
                switch appRootManager.currentAppRoot {
                case .splash:
                    SplashView()
                case .authentication:
                    AuthView()
                        .transition(.opacity)
                case .registerUserName:
                    UpdateUserNameView()
                        .transition(.opacity)
                case .main:
                    MainTabView()
                        .transition(.opacity)
                }
            }
            .animation(.default, value: appRootManager.currentAppRoot)
            .environment(appRootManager)
        }
    }
}
