//
//  SearchService.swift
//  HungNotes
//
//  Created by Hung Cao on 24/5/24.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase

class SearchService {
    private var dbRef: DatabaseReference
    
    init() {
        dbRef = Database.database(url: Constants.DATABASE_URL).reference()
    }
    
    func searchUsers(by searchText: String) async throws -> [User] {
        let myEmail = Auth.auth().currentUser?.email ?? ""
        if searchText.isEmpty { return [] }
        do {
            let snapshot = try await dbRef.child("users").getData()
            let dict = snapshot.value as? [String: Any] ?? [:]
            var users: [User] = []
            for (_, value) in dict {
                let userDict = value as? [String: String] ?? [:]
                let username = userDict["username"] ?? ""
                let email = userDict["email"] ?? ""
                if !myEmail.isEmpty && email != myEmail {
                    users.append(User(username: username, email: email))
                }
            }
            users = users.filter { ($0.username.contains(searchText) || $0.email.contains(searchText)) }
            return users
        } catch {
            throw error
        }
    }
}
