//
//  UserNoteViewModel.swift
//  HungNotes
//
//  Created by Hung Cao on 24/5/24.
//

import Foundation

@Observable
class UserNoteViewModel: BaseViewModel {
    var user: User
    var groupedNotes: [GroupedNote] = []
    var selectedNote: Note?
    
    private let service = NoteService()
    
    init(user: User) {
        self.user = user
    }
    
    func getNotes() {
        Task {
            do {
                let notes = try await service.getUserNote(by: user.email)
                let groupDict = Dictionary(grouping: notes) { (note) -> DateComponents in
                    let date = Calendar.current.dateComponents([.day, .year, .month], from: note.date)
                    return date
                }
                self.groupedNotes = groupDict.map { dateComponents, notes in
                    let calendar = Calendar.current
                    let date = calendar.date(from: dateComponents) ?? Date()
                    return GroupedNote(date: date, notes: notes)
                }
                self.groupedNotes = self.groupedNotes.sorted { $0.date > $1.date }
            } catch {
                self.groupedNotes = []
                print(error.localizedDescription)
            }
        }
    }
}
