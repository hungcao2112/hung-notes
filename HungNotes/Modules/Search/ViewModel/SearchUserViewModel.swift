//
//  SearchUserViewModel.swift
//  HungNotes
//
//  Created by Hung Cao on 24/5/24.
//

import Foundation
import Combine

@Observable
class SearchUserViewModel {
    var searchText: String = ""
    var searchTextPublisher = PassthroughSubject<String, Never>()
    var selectedUser: User?
    var users: [User] = []
    
    private var service = SearchService()
    
    private var bag = Set<AnyCancellable>()
    
    func searchUsers(by searchText: String) {
        Task {
            do {
                users = try await service.searchUsers(by: searchText)
            } catch {
                users = []
                print(error.localizedDescription)
            }
        }
    }
}
