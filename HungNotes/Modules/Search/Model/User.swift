//
//  User.swift
//  HungNotes
//
//  Created by Hung Cao on 24/5/24.
//

import Foundation

struct User: Identifiable, Hashable {
    var id = UUID()
    let username: String
    let email: String
}
