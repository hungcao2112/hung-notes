//
//  UserNoteView.swift
//  HungNotes
//
//  Created by Hung Cao on 24/5/24.
//

import SwiftUI

struct UserNoteView: View {
    @State private var viewModel: UserNoteViewModel
    
    init(viewModel: UserNoteViewModel) {
        self.viewModel = viewModel
        self.viewModel.getNotes()
    }
    
    var body: some View {
        BaseView {
            VStack(alignment: .leading, spacing: 20) {
                Text("\(viewModel.user.username)'s notes")
                    .font(.system(size: 28, weight: .bold))
                    .padding(.horizontal, 20)
                
                Text("Email: \(viewModel.user.email)")
                    .font(.system(size: 16))
                    .foregroundStyle(Color.gray)
                    .padding(.horizontal, 20)
                
                List {
                    ForEach(viewModel.groupedNotes) { groupNote in
                        Section(header: Text(groupNote.date.toDateString())) {
                            ForEach(groupNote.notes) { note in
                                HStack {
                                    Text(note.title)
                                        .font(.system(size: 16))
                                    Spacer()
                                    Text(note.date.toTimeString())
                                        .font(.system(size: 14))
                                        .foregroundStyle(Color.gray)
                                }
                                .contentShape(Rectangle())
                                .onTapGesture {
                                    viewModel.selectedNote = note
                                }
                            }
                        }
                    }
                }
                .listStyle(.plain)
                .refreshable {
                    viewModel.getNotes()
                }
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(.top, 20)
        }
        .navigationDestination(item: $viewModel.selectedNote) { note in
            NoteDetailView(viewModel: .init(note: note))
        }
    }
}

#Preview {
    UserNoteView(
        viewModel: .init(
            user: .init(
                username: "Hung",
                email: "hung@gmail.com")
        )
    )
}
