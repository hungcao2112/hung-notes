//
//  SearchUserView.swift
//  HungNotes
//
//  Created by Hung Cao on 24/5/24.
//

import SwiftUI
import Combine

struct SearchUserView: View {
    @State private var viewModel = SearchUserViewModel()
    
    var body: some View {
        NavigationView {
            VStack {
                if viewModel.users.isEmpty {
                    EmptyView(emptyMessage: "No user have been found yet")
                } else {
                    List {
                        ForEach(viewModel.users) { user in
                            VStack(alignment: .leading) {
                                Text(user.username)
                                    .font(.system(size: 16, weight: .medium))
                                
                                Spacer().frame(height: 10)
                                
                                Text(user.email)
                                    .font(.system(size: 16))
                                    .foregroundStyle(Color.gray)
                            }
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .contentShape(Rectangle())
                            .onTapGesture {
                                viewModel.selectedUser = user
                            }
                        }
                    }
                    .listStyle(.plain)
                }
            }
            .navigationBarTitleDisplayMode(.large)
            .navigationTitle("Search users")
            .toolbarBackground(Color.white, for: .navigationBar)
            
        }
        .searchable(text: $viewModel.searchText, prompt: "Search by username or email")
        .onChange(of: viewModel.searchText) { oldValue, newValue in
            viewModel.searchTextPublisher.send(newValue)
        }
        .onReceive(
            viewModel.searchTextPublisher.debounce(
                for: .seconds(0.5),
                scheduler: DispatchQueue.main
            )
        ) { searchText in
            viewModel.searchUsers(by: searchText)
        }
        .navigationDestination(item: $viewModel.selectedUser) { user in
            UserNoteView(viewModel: .init(user: user))
        }
    }
}

#Preview {
    SearchUserView()
}
