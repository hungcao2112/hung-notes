//
//  RegisterView.swift
//  HungNotes
//
//  Created by Hung Cao on 22/5/24.
//

import SwiftUI

struct RegisterView: View {
    @Environment(AppRootManager.self) private var appRootManager
    @State private var viewModel = RegisterViewModel()
    
    var body: some View {
        BaseView {
            VStack(alignment: .leading) {
                VStack(alignment: .leading, spacing: 16) {
                    Text("Register")
                        .font(.system(size: 24, weight: .bold))
                    
                    Text("Enter email and password to create your account")
                        .font(.system(size: 16, weight: .regular))
                        .foregroundStyle(Color.gray)
                }
                
                Spacer()
                
                VStack(spacing: 16) {
                    FormTextField(
                        label: "",
                        placeholder: "Email",
                        textFieldValue: $viewModel.email,
                        keyboardType: .emailAddress
                    )
                    
                    FormTextField(
                        label: "",
                        placeholder: "Password",
                        textFieldValue: $viewModel.password,
                        keyboardType: .default,
                        isSecured: true
                    )
                    
                    FormTextField(
                        label: "",
                        placeholder: "Confirm Password",
                        textFieldValue: $viewModel.confirmPassword,
                        keyboardType: .default,
                        isSecured: true
                    )
                    
                    Button {
                        viewModel.register()
                    } label: {
                        Text("Register")
                            .font(.system(size: 16, weight: .medium))
                            .foregroundStyle(Color.white)
                            .padding(.horizontal, 16)
                            .frame(maxWidth: .infinity, maxHeight: 50)
                    }
                    .background(
                        Capsule()
                            .fill(.teal)
                    )
                    .padding(.top, 50)
                }
                
                Spacer()
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(20)
            .alert(
                viewModel.errorMessage,
                isPresented: $viewModel.showError
            ) {
                Button("OK", role: .cancel) { }
            }
            .overlay {
                ActivityLoader(isLoading: $viewModel.showLoading)
            }
            .onChange(of: viewModel.authResult) { oldValue, newValue in
                if viewModel.isUsernameRegistered {
                    appRootManager.currentAppRoot = .main
                } else {
                    appRootManager.currentAppRoot = .registerUserName
                }
            }
        }
    }
}

#Preview {
    RegisterView()
        .environment(AppRootManager())
}
