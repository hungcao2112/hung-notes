//
//  UpdateUserNameView.swift
//  HungNotes
//
//  Created by Hung Cao on 22/5/24.
//

import SwiftUI

struct UpdateUserNameView: View {
    @Environment(AppRootManager.self) private var appRootManager
    @State private var viewModel = UpdateUserNameViewModel()
    
    var body: some View {
        VStack(alignment: .leading) {
            VStack(alignment: .leading, spacing: 16) {
                Text("Setup your user name")
                    .font(.system(size: 24, weight: .bold))
                
                Text("What should we call you ?")
                    .font(.system(size: 16, weight: .regular))
                    .foregroundStyle(Color.gray)
            }
            .padding(.top, 50)
            
            Spacer()
            
            VStack(spacing: 16) {
                FormTextField(
                    label: "",
                    placeholder: "Enter your username",
                    textFieldValue: $viewModel.userName,
                    keyboardType: .emailAddress
                )
                
                Button {
                    viewModel.setUserName()
                } label: {
                    Text("Update")
                        .font(.system(size: 16, weight: .medium))
                        .foregroundStyle(Color.white)
                        .padding(.horizontal, 16)
                        .frame(maxWidth: .infinity, maxHeight: 50)
                }
                .background(
                    Capsule()
                        .fill(.teal)
                )
                .padding(.top, 50)
            }
            
            Spacer()
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .padding(20)
        .alert(
            viewModel.errorMessage,
            isPresented: $viewModel.showError
        ) {
            Button("OK", role: .cancel) { }
        }
        .overlay {
            ActivityLoader(isLoading: $viewModel.showLoading)
        }
        .onChange(of: viewModel.completed) { oldValue, newValue in
            if newValue {
                appRootManager.currentAppRoot = .main
            }
        }
    }
}

#Preview {
    UpdateUserNameView()
        .environment(AppRootManager())
}
