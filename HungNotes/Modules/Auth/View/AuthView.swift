//
//  AuthView.swift
//  HungNotes
//
//  Created by Hung Cao on 22/5/24.
//

import SwiftUI

struct AuthView: View {
    var body: some View {
        NavigationStack {
            VStack(spacing: 100) {
                VStack(spacing: 32) {
                    Image(systemName: "book.pages")
                        .resizable()
                        .frame(width: 100, height: 100)
                        .foregroundStyle(Color.teal)
                    
                    Text("Welcome to Hung Notes")
                        .font(.system(size: 24, weight: .bold))
                }
                
                VStack(spacing: 16) {
                    NavigationLink {
                        LoginView()
                    } label: {
                        Text("Login")
                            .font(.system(size: 16, weight: .medium))
                            .foregroundStyle(Color.white)
                            .padding(.horizontal, 16)
                            .frame(maxWidth: .infinity, maxHeight: 50)
                    }
                    .background(
                        Capsule()
                            .fill(.teal)
                    )
                    .padding(.horizontal, 16)
                    
                    NavigationLink {
                        RegisterView()
                    } label: {
                        Text("Register")
                            .font(.system(size: 16, weight: .medium))
                            .foregroundStyle(.teal)
                            .padding(.horizontal, 16)
                            .frame(maxWidth: .infinity, maxHeight: 50)
                    }
                    .background(
                        Capsule()
                            .stroke(Color.teal)
                            .fill(.white)
                    )
                    .padding(.horizontal, 16)
                }
                
            }
        }
    }
}

#Preview {
    AuthView()
}
