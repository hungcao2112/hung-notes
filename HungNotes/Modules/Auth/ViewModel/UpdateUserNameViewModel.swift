//
//  UpdateUserNameViewModel.swift
//  HungNotes
//
//  Created by Hung Cao on 22/5/24.
//

import Foundation

@Observable
class UpdateUserNameViewModel: BaseViewModel {
    var userName: String = ""
    var completed: Bool = false
    
    private let service = AuthService.shared
    
    func setUserName() {
        if userName.isEmpty {
            showError = true
            errorMessage = "User name must not be empty"
            return
        }
        if !userName.isValidUsername() {
            showError = true
            errorMessage = "User name must not contain special characters"
            return
        }
        service.updateUserName(userName: userName)
        completed = true
    }
}
