//
//  LoginViewModel.swift
//  HungNotes
//
//  Created by Hung Cao on 22/5/24.
//

import Foundation
import FirebaseAuth

@Observable
class LoginViewModel: BaseViewModel {
    var email: String = ""
    var password: String = ""
    var authResult: AuthDataResult?
    
    private var service = AuthService.shared
    
    var isUsernameRegistered: Bool {
        return service.isUsernameRegistered()
    }
    
    func login() {
        if !email.isValidEmail() {
            showError = true
            errorMessage = "Invalid email"
            return
        }
        showLoading = true
        Task {
            do {
                authResult = try await service.login(email: email, password: password)
                showLoading = false
            } catch {
                showError = true
                errorMessage = error.localizedDescription
                showLoading = false
            }
        }
    }
}
