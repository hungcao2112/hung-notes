//
//  RegisterViewModel.swift
//  HungNotes
//
//  Created by Hung Cao on 22/5/24.
//

import Foundation
import FirebaseAuth

@Observable
class RegisterViewModel: BaseViewModel {
    var email: String = ""
    var password: String = ""
    var confirmPassword: String = ""
    var authResult: AuthDataResult?
    
    private var service = AuthService.shared
    
    var isUsernameRegistered: Bool {
        return service.isUsernameRegistered()
    }
    
    func register() {
        if !email.isValidEmail() {
            showError = true
            errorMessage = "Invalid email"
            return
        }
        if password != confirmPassword {
            showError = true
            errorMessage = "Confirm password must be the same with password"
            return
        }
        showLoading = true
        Task {
            do {
                authResult = try await service.register(email: email, password: password)
                showLoading = false
            } catch {
                showError = true
                errorMessage = error.localizedDescription
                showLoading = false
            }
        }
    }
}
