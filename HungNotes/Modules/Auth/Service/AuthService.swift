//
//  AuthService.swift
//  HungNotes
//
//  Created by Hung Cao on 22/5/24.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase

class AuthService {
    static let shared = AuthService()
    
    private var dbRef: DatabaseReference
    
    private init() {
        dbRef = Database.database(url: Constants.DATABASE_URL).reference()
    }
    
    func register(email: String, password: String) async throws -> AuthDataResult {
        do {
            let result = try await Auth.auth().createUser(withEmail: email, password: password)
            UserDefaults.standard.set(true, forKey: "isLoggedIn")
            return result
        } catch {
            throw error
        }
    }
    
    func login(email: String, password: String) async throws -> AuthDataResult {
        do {
            let result = try await Auth.auth().signIn(withEmail: email, password: password)
            UserDefaults.standard.set(true, forKey: "isLoggedIn")
            return result
        } catch {
            throw error
        }
    }
    
    func updateUserName(userName: String) {
        guard let currentUser = Auth.auth().currentUser, let email = currentUser.email else { return }
        dbRef.child("users").child(email.formatEmail()).setValue([
            "username": userName,
            "email": email
        ])
        
        let changeRequest = currentUser.createProfileChangeRequest()
        changeRequest.displayName = userName
        changeRequest.commitChanges()
    }
}

extension AuthService {
    func isUsernameRegistered() -> Bool {
        return Auth.auth().currentUser?.displayName != nil
    }
}
