//
//  SplashView.swift
//  HungNotes
//
//  Created by Hung Cao on 22/5/24.
//

import SwiftUI

struct SplashView: View {
    @Environment(AppRootManager.self) private var appRootManager
    private var viewModel = SplashViewModel()
    
    var body: some View {
        ZStack {
            Image(systemName: "book.pages")
                .resizable()
                .scaledToFit()
                .frame(width: 150, height: 150)
                .foregroundStyle(Color.teal)
        }
        .onAppear {
            appRootManager.currentAppRoot = viewModel.appRoot
        }
    }
}

#Preview {
    SplashView()
        .environment(AppRootManager())
}
