//
//  SplashViewModel.swift
//  HungNotes
//
//  Created by Hung Cao on 22/5/24.
//

import Foundation
import FirebaseAuth

class SplashViewModel {
    var appRoot: AppRootManager.AppRoot {
        let isLoggedIn = UserDefaults.standard.bool(forKey: "isLoggedIn")
        guard isLoggedIn, let currentUser = Auth.auth().currentUser else {
            return .authentication
        }
        guard currentUser.displayName != nil else {
            return .registerUserName
        }
        return .main
    }
}
