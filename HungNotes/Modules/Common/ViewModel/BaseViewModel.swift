//
//  BaseViewModel.swift
//  HungNotes
//
//  Created by Hung Cao on 22/5/24.
//

import Foundation

@Observable
class BaseViewModel {
    var showLoading: Bool = false
    var showError: Bool = false
    var errorMessage: String = ""
}
