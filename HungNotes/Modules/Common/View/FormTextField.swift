//
//  FormTextField.swift
//  HungNotes
//
//  Created by Hung Cao on 22/5/24.
//

import SwiftUI

import SwiftUI

struct FormTextField: View {
    var label: String
    var placeholder: String
    @Binding var textFieldValue: String
    var keyboardType: UIKeyboardType = .default
    var isSecured: Bool = false
    var textFieldHeight: CGFloat = 44
    var allowsMultiline: Bool = false
    
    private let textLimit: Int = 150
    
    @FocusState private var isFocused
    
    
    var body: some View {
        VStack(alignment: .leading, spacing: 16) {
            if !label.isEmpty {
                Text(label)
                    .font(.system(size: 16, weight: .medium))
            }
            
            VStack {
                ZStack {
                    if isSecured {
                        SecureField(placeholder, text: $textFieldValue)
                            .keyboardType(keyboardType)
                            .font(.system(size: 16))
                            .padding(.horizontal, 8)
                            .focused($isFocused)
                            .textInputAutocapitalization(.never)
                    } else {
                        TextField(placeholder, text: $textFieldValue, axis: allowsMultiline ? .vertical : .horizontal)
                            .onChange(of: textFieldValue, { oldValue, newValue in
                                self.textFieldValue = String(newValue.prefix(textLimit))
                            })
                            .keyboardType(keyboardType)
                            .font(.system(size: 16))
                            .padding(8)
                            .focused($isFocused)
                            .textInputAutocapitalization(.never)
                    }
                }
                .frame(height: textFieldHeight, alignment: allowsMultiline ? .top : .center)
                .background(
                    RoundedRectangle(cornerRadius: 8)
                        .fill(Color.white)
                        .stroke(isFocused ? .teal : Color(uiColor: .systemGray4))
                )
                .onTapGesture {
                    isFocused = true
                }
                
                if allowsMultiline {
                    Text("\(textFieldValue.count)/\(textLimit)")
                        .font(.system(size: 14))
                        .foregroundStyle(Color.gray)
                        .frame(maxWidth: .infinity, alignment: .trailing)
                }
            }
        }
    }
}

#Preview {
    FormTextField(
        label: "Phone number",
        placeholder: "Enter phone number",
        textFieldValue: .constant(""),
        textFieldHeight: 100,
        allowsMultiline: true
    )
}
