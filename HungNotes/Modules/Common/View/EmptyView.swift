//
//  EmptyView.swift
//  HungNotes
//
//  Created by Hung Cao on 24/5/24.
//

import SwiftUI

struct EmptyView: View {
    let emptyMessage: String
    
    init(emptyMessage: String) {
        self.emptyMessage = emptyMessage
    }
    
    var body: some View {
        ZStack {
            Text(emptyMessage)
                .font(.system(size: 16))
                .foregroundStyle(Color.gray)
                .padding(.horizontal, 20)
                .multilineTextAlignment(.center)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color.clear)
    }
}

#Preview {
    EmptyView(emptyMessage: "This is empty")
}
