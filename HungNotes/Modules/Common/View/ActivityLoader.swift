//
//  ActivityLoader.swift
//  HungNotes
//
//  Created by Hung Cao on 22/5/24.
//

import SwiftUI

struct ActivityLoader: View {
    @Binding var isLoading: Bool

    private enum Metrics {
        static let opacity: CGFloat = 0.2
        static let scaleEffect: CGFloat = 2.0
    }

    var body: some View {
        ZStack {
            ProgressView()
                .progressViewStyle(.circular)
                .scaleEffect(Metrics.scaleEffect)
                .frame(width: 100, height: 100)
                .background(Color(uiColor: .systemGroupedBackground))
                .clipShape(
                    RoundedRectangle(cornerRadius: 16.0)
                )
                .tint(.gray)
        }
        .opacity(isLoading ? 1.0 : 0)
        .animation(.default, value: isLoading)
    }
}

#Preview {
    ActivityLoader(isLoading: .constant(true))
}
