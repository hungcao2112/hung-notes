//
//  ProfileService.swift
//  HungNotes
//
//  Created by Hung Cao on 24/5/24.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase

class ProfileService {
    private var dbRef: DatabaseReference
    
    init() {
        dbRef = Database.database(url: Constants.DATABASE_URL).reference()
    }
    
    func getProfile() async throws -> User? {
        let myEmail = Auth.auth().currentUser?.email ?? ""
        if myEmail.isEmpty { return nil }
        do {
            let snapshot = try await dbRef.child("users").child(myEmail.formatEmail()).getData()
            let userDict = snapshot.value as? [String: String] ?? [:]
            let username = userDict["username"] ?? ""
            let email = userDict["email"] ?? ""
            return User(username: username, email: email)
        } catch {
            throw error
        }
    }
    
    func updateProfile(username: String, completion: (()->())?) {
        guard let currentUser = Auth.auth().currentUser, let email = currentUser.email else { return }
        dbRef.child("users").child(email.formatEmail()).setValue([
            "username": username,
            "email": email
        ])
        let changeRequest = currentUser.createProfileChangeRequest()
        changeRequest.displayName = username
        changeRequest.commitChanges { _ in
            completion?()
        }
    }
    
    func logout() {
        let auth = Auth.auth()
        do {
            try auth.signOut()
            UserDefaults.standard.set(true, forKey: "isLoggedIn")
        } catch let signOutError as NSError {
            print("Error signing out: %@", signOutError)
        }
    }
}
