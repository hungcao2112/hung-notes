//
//  ProfileViewModel.swift
//  HungNotes
//
//  Created by Hung Cao on 24/5/24.
//

import Foundation

@Observable
class ProfileViewModel: BaseViewModel {
    var user: User?
    var allowEditing: Bool = false
    var editedUsername: String = ""
    
    private let service = ProfileService()
    
    func getProfile() {
        Task {
            do {
                user = try await service.getProfile()
                editedUsername = user?.username ?? ""
            } catch {
                user = nil
                print(error.localizedDescription)
            }
        }
    }
    
    func updateProfile() {
        showLoading = true
        service.updateProfile(username: editedUsername) { [weak self] in
            guard let self else { return }
            self.getProfile()
            self.showLoading = false
        }
    }
    
    func logout() {
        service.logout()
    }
}
