//
//  ProfileView.swift
//  HungNotes
//
//  Created by Hung Cao on 24/5/24.
//

import SwiftUI

struct ProfileView: View {
    @Environment(AppRootManager.self) private var appRootManager
    @State private var viewModel = ProfileViewModel()
    
    @FocusState private var isFocused
    
    init() {
        viewModel.getProfile()
    }
    
    var body: some View {
        NavigationView {
            VStack(alignment: .leading, spacing: 36) {
                VStack(alignment: .leading, spacing: 10) {
                    Text("Username")
                        .font(.system(size: 16, weight: .medium))
                    
                    HStack(spacing: 10) {
                        if viewModel.allowEditing {
                            TextField("Enter new username", text: $viewModel.editedUsername)
                                .textFieldStyle(.roundedBorder)
                                .font(.system(size: 16))
                                .focused($isFocused)
                        } else {
                            Text(viewModel.user?.username ?? "")
                                .font(.system(size: 16))
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .foregroundStyle(Color.gray)
                        }
                        
                        if viewModel.allowEditing {
                            Button("Save") {
                                viewModel.allowEditing = false
                                isFocused = false
                                viewModel.updateProfile()
                            }
                            .tint(Color.teal)
                            .disabled(viewModel.editedUsername.isEmpty)
                        } else {
                            Button {
                                viewModel.allowEditing = true
                                isFocused = true
                            } label: {
                                Image(systemName: "pencil.line")
                                    .resizable()
                                    .frame(width: 24, height: 24)
                                    .foregroundStyle(Color.teal)
                            }
                        }
                    }
                }
                
                VStack(alignment: .leading, spacing: 10) {
                    Text("Email")
                        .font(.system(size: 16, weight: .medium))
                    Text(viewModel.user?.email ?? "")
                        .font(.system(size: 16))
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .foregroundStyle(Color.gray)
                }
                
                Spacer()
                
                Button {
                    viewModel.logout()
                    appRootManager.currentAppRoot = .authentication
                } label: {
                    Text("Logout")
                        .font(.system(size: 16, weight: .medium))
                        .foregroundStyle(Color.teal)
                        .padding(.horizontal, 16)
                        .frame(maxWidth: .infinity, maxHeight: 50)
                }
                .background(
                    RoundedRectangle(cornerRadius: 16)
                        .fill(Color.teal.opacity(0.3))
                )
                .padding(.bottom, 20)
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(.horizontal, 20)
            .padding(.top, 50)
            .navigationBarTitleDisplayMode(.large)
            .navigationTitle("Profile")
            .toolbarBackground(Color.white, for: .navigationBar)
            .overlay {
                ActivityLoader(isLoading: $viewModel.showLoading)
            }
        }
    }
}

#Preview {
    ProfileView()
        .environment(AppRootManager())
}
