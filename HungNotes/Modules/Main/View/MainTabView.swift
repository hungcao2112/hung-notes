//
//  MainTabView.swift
//  HungNotes
//
//  Created by Hung Cao on 23/5/24.
//

import SwiftUI

struct MainTabView: View {
    @State var tabManager: TabManager = TabManager()
    
    var body: some View {
        NavigationStack {
            ZStack {
                TabView(selection: $tabManager.currentTab) {
                    ForEach(tabManager.allTabs, id: \.self) { tab in
                        getChildView(by: tab)
                            .tag(tabManager.currentTab.rawValue)
                            .tabItem {
                                Label(tab.title, systemImage: tab.icon)
                            }
                    }
                    .toolbarBackground(Color.white, for: .tabBar)
                }
                .tint(.teal)
            }
        }
    }
    
    @ViewBuilder func getChildView(by tab: TabManager.Tab) -> some View {
        switch tab {
        case .notes:
            NoteListView()
        case .search:
            SearchUserView()
        case .profile:
            ProfileView()
        }
    }
}

#Preview {
    MainTabView()
}
