//
//  Note.swift
//  HungNotes
//
//  Created by Hung Cao on 23/5/24.
//

import Foundation

struct Note: Identifiable, Hashable {
    var id = UUID()
    let title: String
    let content: String
    let date: Date
}

struct GroupedNote: Identifiable {
    let id = UUID()
    let date: Date
    let notes: [Note]
}
