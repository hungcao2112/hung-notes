//
//  NoteService.swift
//  HungNotes
//
//  Created by Hung Cao on 23/5/24.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase

class NoteService {
    private var dbRef: DatabaseReference
    
    init() {
        dbRef = Database.database(url: Constants.DATABASE_URL).reference()
    }
    
    func addNote(title: String, content: String, date: Date) {
        guard let currentUser = Auth.auth().currentUser, let email = currentUser.email else { return }
        let dateFormatter = DateFormatter.iso8601
        let dateStr = dateFormatter.string(from: date)
        dbRef.child("note_list").child(email.formatEmail()).child(dateStr)
            .setValue([
                "title": title,
                "content": content
            ])
    }
    
    func getNotes(completion: @escaping (([Note])->())) {
        guard let currentUser = Auth.auth().currentUser, let email = currentUser.email else { return }
        
        dbRef.child("note_list").child(email.formatEmail()).observe(.value) { snapshot in
            let value = snapshot.value as? [String: Any] ?? [:]
            let dateFormatter = DateFormatter.iso8601
            var notes: [Note] = []
            for (key, data) in value {
                let date = dateFormatter.date(from: key) ?? Date()
                let title = (data as? [String: String])?["title"] ?? ""
                let content = (data as? [String: String])?["content"] ?? ""
                notes.append(Note(title: title, content: content, date: date))
            }
            completion(notes.sorted { $0.date > $1.date })
        }
    }
    
    func getUserNote(by email: String) async throws -> [Note] {
        if email.isEmpty { return [] }
        do {
            let snapshot = try await dbRef.child("note_list").child(email.formatEmail()).getData()
            let value = snapshot.value as? [String: Any] ?? [:]
            let dateFormatter = DateFormatter.iso8601
            var notes: [Note] = []
            for (key, data) in value {
                let date = dateFormatter.date(from: key) ?? Date()
                let title = (data as? [String: String])?["title"] ?? ""
                let content = (data as? [String: String])?["content"] ?? ""
                notes.append(Note(title: title, content: content, date: date))
            }
            return notes.sorted { $0.date > $1.date }
        } catch {
            throw error
        }
    }
}
