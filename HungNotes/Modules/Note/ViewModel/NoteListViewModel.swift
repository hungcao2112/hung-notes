//
//  NoteListViewModel.swift
//  HungNotes
//
//  Created by Hung Cao on 23/5/24.
//

import Foundation
import FirebaseAuth

@Observable
class NoteListViewModel: BaseViewModel {
    var username: String = ""
    var groupedNotes: [GroupedNote] = []
    var selectedNote: Note?
    
    private var service = NoteService()
    
    override init() {
        username = Auth.auth().currentUser?.displayName ?? ""
    }
    
    func getNotes() {
        service.getNotes { [weak self] notes in
            guard let self else { return }
            let groupDict = Dictionary(grouping: notes) { (note) -> DateComponents in
                let date = Calendar.current.dateComponents([.day, .year, .month], from: note.date)
                return date
            }
            self.groupedNotes = groupDict.map { dateComponents, notes in
                let calendar = Calendar.current
                let date = calendar.date(from: dateComponents) ?? Date()
                return GroupedNote(date: date, notes: notes)
            }
            self.groupedNotes = self.groupedNotes.sorted { $0.date > $1.date }
        }
    }
}
