//
//  AddNoteViewModel.swift
//  HungNotes
//
//  Created by Hung Cao on 23/5/24.
//

import Foundation

@Observable
class AddNoteViewModel {
    var title: String = ""
    var content: String = ""
    var date: Date = Date()
    
    private var service = NoteService()
    
    func addNote() {
        service.addNote(
            title: title,
            content: content,
            date: date
        )
    }
}
