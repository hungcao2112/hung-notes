//
//  NoteDetailViewModel.swift
//  HungNotes
//
//  Created by Hung Cao on 23/5/24.
//

import Foundation

@Observable
class NoteDetailViewModel: BaseViewModel {
    var note: Note
    
    init(note: Note) {
        self.note = note
    }
}
