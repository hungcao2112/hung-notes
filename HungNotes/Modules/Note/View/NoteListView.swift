//
//  NoteListView.swift
//  HungNotes
//
//  Created by Hung Cao on 23/5/24.
//

import SwiftUI

struct NoteListView: View {
    @State private var viewModel = NoteListViewModel()
    
    @State private var presentAddNote: Bool = false
    
    init() {
        viewModel.getNotes()
    }
    
    var body: some View {
        NavigationView {
            ZStack {
                if viewModel.groupedNotes.isEmpty {
                    EmptyView(emptyMessage: "You haven't created any note yet. Tap on the \"+\" button to start creating new note")
                } else {
                    List {
                        ForEach(viewModel.groupedNotes) { groupNote in
                            Section(header: Text(groupNote.date.toDateString())) {
                                ForEach(groupNote.notes) { note in
                                    HStack {
                                        Text(note.title)
                                            .font(.system(size: 16))
                                        Spacer()
                                        Text(note.date.toTimeString())
                                            .font(.system(size: 14))
                                            .foregroundStyle(Color.gray)
                                    }
                                    .contentShape(Rectangle())
                                    .onTapGesture {
                                        viewModel.selectedNote = note
                                    }
                                }
                            }
                        }
                    }
                    .listStyle(.grouped)
                }
            }
            .navigationBarTitleDisplayMode(.large)
            .navigationTitle("Your Notes")
            .toolbarBackground(Color.white, for: .navigationBar)
            .toolbar {
                ToolbarItem(placement: .topBarTrailing) {
                    Button {
                        presentAddNote = true
                    } label: {
                        Image(systemName: "plus")
                            .resizable()
                            .frame(width: 24, height: 24)
                            .foregroundStyle(Color.teal)
                    }
                }
            }
            .sheet(isPresented: $presentAddNote) {
                AddNoteView()
                    .presentationDetents([.medium])
                    .presentationDragIndicator(.hidden)
            }
        }
        .navigationDestination(item: $viewModel.selectedNote) { note in
            NoteDetailView(viewModel: .init(note: note))
        }
    }
}

#Preview {
    NoteListView()
}
