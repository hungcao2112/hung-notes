//
//  NoteDetailView.swift
//  HungNotes
//
//  Created by Hung Cao on 23/5/24.
//

import SwiftUI

struct NoteDetailView: View {
    @State private var viewModel: NoteDetailViewModel
    
    init(viewModel: NoteDetailViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        BaseView {
            VStack(alignment: .leading, spacing: 20) {
                Text(viewModel.note.title)
                    .font(.system(size: 32, weight: .bold))
                
                Text("Created at \(viewModel.note.date.toDateTimeString())")
                    .font(.system(size: 16))
                    .foregroundStyle(Color.gray)
                
                Text(viewModel.note.content)
                    .font(.system(size: 16))
                
                Spacer()
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(20)
        }
        .navigationBarTitleDisplayMode(.inline)
    }
}

#Preview {
    NoteDetailView(
        viewModel: .init(
            note: Note(
                title: "AAA",
                content: "BBB",
                date: Date()
            )
        )
    )
}
