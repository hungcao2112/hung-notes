//
//  AddNoteView.swift
//  HungNotes
//
//  Created by Hung Cao on 23/5/24.
//

import SwiftUI

struct AddNoteView: View {
    @Environment(\.dismiss) var dismiss
    @State private var viewModel = AddNoteViewModel()
    
    var body: some View {
        NavigationStack {
            VStack(spacing: 16) {
                FormTextField(
                    label: "Title",
                    placeholder: "Enter a title for your note",
                    textFieldValue: $viewModel.title)
                
                FormTextField(
                    label: "Content",
                    placeholder: "Write a short note with max 150 words",
                    textFieldValue: $viewModel.content,
                    textFieldHeight: 100,
                    allowsMultiline: true
                )
                
                Spacer()
            }
            .padding(20)
            .navigationBarTitleDisplayMode(.inline)
            .navigationTitle("Add New Note")
            .toolbarBackground(Color.white, for: .navigationBar)
            .toolbar {
                ToolbarItem(placement: .topBarTrailing) {
                    Button("Save") {
                        viewModel.addNote()
                        dismiss()
                    }
                    .tint(Color.teal)
                    .disabled(viewModel.title.isEmpty || viewModel.content.isEmpty)
                }
            }
        }
    }
}

#Preview {
    AddNoteView()
}
