//
//  TabManager.swift
//  HungNotes
//
//  Created by Hung Cao on 23/5/24.
//

import Foundation

@Observable
class TabManager {
    var currentTab: Tab = .notes
    
    var allTabs: [Tab] {
        Tab.allCases
    }
    
    enum Tab: Int, CaseIterable {
        case notes = 0
        case search = 1
        case profile = 2
        
        var icon: String {
            switch self {
            case .notes:
                "book.pages"
            case .search:
                "magnifyingglass"
            case .profile:
                "person.fill"
            }
        }
        
        var title: String {
            switch self {
            case .notes:
                "Notes"
            case .search:
                "Search"
            case .profile:
                "Profile"
            }
        }
    }
}
