//
//  AppRootManager.swift
//  HungNotes
//
//  Created by Hung Cao on 22/5/24.
//

import Foundation

@Observable
class AppRootManager {
    var currentAppRoot: AppRoot = .splash
    
    enum AppRoot {
        case splash
        case authentication
        case registerUserName
        case main
    }
}
