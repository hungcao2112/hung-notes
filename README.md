# Hung Notes

An iOS application which allows users to create notes and check for other user's notes

## 1. How did I approach this project?

After understand the requirements, I indicated the project must have these features:
- Authentication (Login/Register/Logout)
- User's note list
- Add user's new note
- Search for other users
- Checking other user's notes
- Profile scene (to view user profile and edit the username)

### Disadvantages & difficulties:
- This is the first time I've ever work with the Firebase Realtime Database, it takes time at the begining to investigate
- I only had about 1 year working with SwiftUI so sometimes it was a struggle for me about setting up the UIs
- The timeline for this project is limited so some of the features may not be optimized in time
- The biggest issue is that the Firebase database does not allows to save an array of data, so it takes time for me to find a way to organize the data properly and how to correctly retrieve and process the data

### Advantages:
- I've a chance to work with Firebase authentication before so it's not to hard for me to setup the authentication feature
- This project is the good chance for me to learn and practice new techs such as Firebase database, SwiftUI, Combine, ...

## 2. Demo

This is a short video to demonstrate how the app works

[Please check this video on my Google drive](https://drive.google.com/file/d/14cSzEiAPDKe_bmlMDpz8dqDFSqmHCbUP/view?usp=sharing)

## 3. How to install?

1. Pull or clone the project
2. Navigate to project's directory, run `pod install` in the terminal
3. Open project's workspace file, and click the run button